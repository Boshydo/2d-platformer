using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player2: Paddle
{
    public void Update()
    {
        //Assigning keys for the paddle
        if (Input.GetKey(KeyCode.W))
        {
            _rigidbody.velocity = new Vector2(0f, 5f);
            Debug.Log(_rigidbody.velocity);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            _rigidbody.velocity = new Vector2(0f, -5f);
            Debug.Log(_rigidbody.velocity);
        }
        else
        {
            _rigidbody.velocity = new Vector2(0f, 0f);
        }
    }
}   

