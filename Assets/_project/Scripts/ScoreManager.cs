using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    int score;
    public TextMeshProUGUI scoretxt;
    public GameManager GameManager;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Coin")
        {
            //Adding +1 to the score when getting a coin
            score++;
            Destroy(collision.gameObject);
            scoretxt.text = score.ToString();
        }
        if (  collision.gameObject.tag == "Door"&& score >= 12)
        {
            GameManager.YouWinCanvas.SetActive(true);
            Time.timeScale = 0;

        }

    }
    private void Update()
    {
        if (score > PlayerPrefs.GetInt("HighScore"))
        {
            PlayerPrefs.SetInt("Highscore", score);
        }
    }


}
