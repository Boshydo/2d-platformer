using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    
    Animator anim;
    public float fullHealth;
    public float currentHealth;
    public bool playerDied = false;
    public bool isInvulnerable = false;
    [SerializeField] HealthBar healthBar;
    public GameManager gameManager;
  
    public void Awake()
    {
        anim = GetComponent<Animator>();
        currentHealth = fullHealth;
       
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        {
            if (isInvulnerable)
            {
                return;
            }
            if (col.gameObject.tag == "Enemies")
            {
                AddDamage(20);
            }
            if (col.gameObject.tag == "Water")
            {
                AddDamage(100);
            }
            
        }
    }
    
    public void AddDamage(float damage)
    {
        currentHealth -= damage;
         currentHealth = Mathf.Clamp(currentHealth, 0f, fullHealth);
        healthBar.SetHealth(100 * (currentHealth / fullHealth));
        GetComponent<AudioSource>().Play();
       

        if (currentHealth <= 0)
        {
            anim.SetBool("Dead", true);
            playerDied = true;
            GetComponent<PlayerCtrl>().enabled = false;
            GetComponent<AudioSource>().enabled = false;
            gameManager.GameOver();

        }
    }
    public void BonusHealth(float Bonus)
    {
        currentHealth += Bonus;
        currentHealth = Mathf.Clamp(currentHealth, 0f, fullHealth);
        healthBar.SetHealth(100 * (currentHealth / fullHealth));
    }
}

