using UnityEngine;

public class TrapMovementRight : MonoBehaviour
{
    public int direction = 1;
    private Vector3 startPosition;
    [SerializeField] public float frequency = 5f;
    [SerializeField] public float magnitude = 5f;
    [SerializeField] public float offset = 0f;
    private void Start()
    {
        startPosition = transform.position;
    }
    private void Update()
    {
        transform.position = startPosition + direction*transform.right * Mathf.Sin(Time.time * frequency + offset) * magnitude;
    }
}
